package com.scorpionglitch.gamemanager.objects;

import java.net.URL;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Company {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(columnDefinition = "CHAR(36)")
	private UUID id;
	
	@JsonProperty("abbreviation")
	private String abbreviation;
	
	@JsonProperty("aliases")
	private String aliases;
	
	@JsonProperty("date_founded")
	private String dateFounded;
	
	@JsonProperty("deck")
	private String deck;
	
	@Column(columnDefinition = "MEDIUMTEXT")
	@JsonProperty("description")
	private String description;

	@JsonProperty("image")
	private URL image;
	
	@JsonProperty("location_address")
	private String locationAddress;
	
	@JsonProperty("location_city")
	private String locationCity;
	
	@JsonProperty("location_country")
	private String locationCountry;
	
	@JsonProperty("location_state")
	private String locationState;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("phone")
	private String phone;

	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	
	@JsonProperty("website")
	private URL website;
	
	private String giantbombID;
}
