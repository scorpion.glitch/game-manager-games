package com.scorpionglitch.gamemanager.objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

public class DefaultPlatformEmulator {
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "client_id", nullable = false)
	@Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private GMClient client;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "emulator_id", nullable = false)
	@Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private Emulator emulator;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "platform_id", nullable = false)
	@Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private Platform platform;
}
