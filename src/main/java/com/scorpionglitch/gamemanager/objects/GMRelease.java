package com.scorpionglitch.gamemanager.objects;

import java.net.URL;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GMRelease {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(columnDefinition = "CHAR(36)")
	private UUID id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "game_id", nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private Game game;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "platform_id", nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private Platform platform;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Developer",
		joinColumns = { @JoinColumn(name = "game_id", nullable = false, columnDefinition = "VARCHAR(36)") },
		inverseJoinColumns = { @JoinColumn(name = "developer_id", nullable = false, columnDefinition = "VARCHAR(36)") }
	)
	@JsonProperty("developers")
	private Set<Company> developers;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Publisher",
		joinColumns = { @JoinColumn(name = "game_id", nullable = false, columnDefinition = "VARCHAR(36)") },
		inverseJoinColumns = { @JoinColumn(name = "publisher_id", nullable = false, columnDefinition = "VARCHAR(36)") }
	)
	@JsonProperty("publishers")
	private Set<Company> publishers;
	
	@JsonProperty("region")
	private String region;
	
	@JsonProperty("release_date")
	private String releaseDate;
	
	private String giantbombID;
	private URL giantbombDetailURL;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Franchise",
		joinColumns = { @JoinColumn(name = "game_id", nullable = false, columnDefinition = "VARCHAR(36)") },
		inverseJoinColumns = { @JoinColumn(name = "franchise_id", nullable = false, columnDefinition = "VARCHAR(36)") }
	)
	private Set<Franchise> franchises;
	
	private String gameRating;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Genre",
		joinColumns = { @JoinColumn(name = "game_id", nullable = false, columnDefinition = "VARCHAR(36)") },
		inverseJoinColumns = { @JoinColumn(name = "genre_id", nullable = false, columnDefinition = "VARCHAR(36)") }
	)
	private Set<Genre> genres;
	
	private URL coverImage;
}
