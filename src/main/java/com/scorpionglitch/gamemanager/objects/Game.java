package com.scorpionglitch.gamemanager.objects;

import java.net.URL;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Game {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(columnDefinition = "CHAR(36)")
	private UUID id;
	
	@JsonProperty("aliases")
	private String aliases;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("deck")
	private String deck;
	
	@Column(columnDefinition = "MEDIUMTEXT")
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("game_rating")
	private String gameRating;
	
	private String giantbombID;
	private URL giantbombDetailURL;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Franchise",
		joinColumns = { @JoinColumn(name = "game_id") },
		inverseJoinColumns = { @JoinColumn(name = "franchise_id") }
	)
	@JsonProperty("franchises")
	private Set<Franchise> franchises;
	
	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(
		name = "Game_Genre",
		joinColumns = { @JoinColumn(name = "game_id") },
		inverseJoinColumns = { @JoinColumn(name = "genre_id") }
	)
	@JsonProperty("genres")
	private Set<Genre> genres;
}
