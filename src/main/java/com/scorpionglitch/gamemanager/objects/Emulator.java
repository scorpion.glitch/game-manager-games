package com.scorpionglitch.gamemanager.objects;

import java.nio.file.Path;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import com.scorpionglitch.gamemanager.tools.PathConverter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Emulator {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(columnDefinition = "CHAR(36)")
	private UUID id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "client_id", nullable = false)
	private GMClient client;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "platform_id", nullable = false)
	private Platform platform;
	
	@NotNull
	@Column(columnDefinition = "varchar(255)")
	@Convert(converter = PathConverter.class)
	private Path path;
}
