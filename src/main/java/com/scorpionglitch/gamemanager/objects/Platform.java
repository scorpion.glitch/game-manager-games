package com.scorpionglitch.gamemanager.objects;

import java.net.URL;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Platform {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private UUID id;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("abbreviation")
	private String abbreviation;
	
	@ManyToOne
	@JsonProperty("company")
	@Column(updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	@Type(type = "uuid-char")
	private Company company;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("deck")
	private String deck;
	
	@Column(columnDefinition = "MEDIUMTEXT")
	@JsonProperty("description")
	private String description;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("image")
	private URL image;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("name")
	private String name;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("original_price")
	private String originalPrice;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("release_date")
	private String releaseDate;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("site_detail_url")
	private URL siteDetailUrl;
	
	@Column(columnDefinition = "VARCHAR(255)")
	@JsonProperty("giantbomb_id")
	private String giantbombID;
}
