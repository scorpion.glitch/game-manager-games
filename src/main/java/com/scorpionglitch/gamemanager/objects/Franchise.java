package com.scorpionglitch.gamemanager.objects;

import java.net.URL;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Franchise {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
		name = "UUID",
		strategy = "org.hibernate.id.UUIDGenerator"
	)
	@Column(columnDefinition = "CHAR(36)")
	private UUID id;
	
	@JsonProperty("aliases")
	private String aliases;
	
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	
	@JsonProperty("deck")
	private String deck;
	
	@Column(columnDefinition = "MEDIUMTEXT")
	@JsonProperty("description")
	private String description;

	@JsonProperty("image")
	private URL image;
	
	@JsonProperty("name")
	private String name;
	
	private String giantbombID;
}
