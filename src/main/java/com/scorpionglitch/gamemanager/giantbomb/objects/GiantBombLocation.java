package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombLocation.class)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombLocation extends GiantBombSearch {
	@JsonProperty("aliases")
	private String aliases;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("first_appeared_in_game")
	private GiantBombGame firstAppearedInGame;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("name")
	private String name;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
}
