package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombConcept.class)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombConcept extends GiantBombSearch {
	@JsonProperty("aliases")
	private String aliases;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("characters")
	private GiantBombCharacter[] characters;
	@JsonProperty("concepts")
	private GiantBombConcept[] concepts;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("first_appeared_in_franchise")
	private GiantBombFranchise firstAppearedInFranchise;
	@JsonProperty("first_appeared_in_game")
	private GiantBombGame firstAppearedInGame;
	@JsonProperty("franchises")
	private GiantBombFranchise[] franchises;
	@JsonProperty("games")
	private GiantBombGame[] games;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("locations")
	private GiantBombLocation[] locations;
	@JsonProperty("name")
	private String name;
	@JsonProperty("objects")
	private GiantBombObject[] objects;
	@JsonProperty("people")
	private GiantBombPerson[] people;
	@JsonProperty("related_concepts")
	private GiantBombConcept[] relatedConcepts;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
}
