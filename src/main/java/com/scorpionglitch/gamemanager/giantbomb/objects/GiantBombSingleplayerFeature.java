package com.scorpionglitch.gamemanager.giantbomb.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombSingleplayerFeature {
	@JsonProperty("singlePlayerFeature")
	private String singlePlayerFeature;
}
