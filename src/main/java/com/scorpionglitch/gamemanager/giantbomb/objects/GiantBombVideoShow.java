package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombVideoShow {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("title")
	private String title;
	@JsonProperty("position")
	private Integer position;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("logo")
	private GiantBombImage logo;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	@JsonProperty("active")
	private Boolean active;
	@JsonProperty("display_nav")
	private Boolean diplayNav;
	@JsonProperty("latest")
	private GiantBombVideo latest;
	@JsonProperty("premium")
	private Boolean premium;
	@JsonProperty("api_videos_url")
	private URL apiVideosURL;
}
