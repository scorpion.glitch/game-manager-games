package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombGame.class)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombGame extends GiantBombSearch {
	@JsonProperty("aliases")
	private String aliases;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("characters")
	private GiantBombCharacter[] characters;
	@JsonProperty("concepts")
	private GiantBombConcept[] concepts;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("developers")
	private GiantBombCompany[] developers;
	@JsonProperty("expected_release_day")
	private Integer expectedReleaseDay;
	@JsonProperty("expected_release_month")
	private Integer expectedReleaseMonth;
	@JsonProperty("expected_release_quarter")
	private Integer expectedReleaseQuarter;
	@JsonProperty("expected_release_year")
	private Integer expectedReleaseYear;
	@JsonProperty("first_appearance_characters")
	private GiantBombCharacter[] firstAppearanceCharacters;
	@JsonProperty("first_appearance_concepts")
	private GiantBombConcept[] firstAppearanceConcepts;
	@JsonProperty("first_appearance_locations")
	private GiantBombLocation[] firstAppearanceLocations;
	@JsonProperty("first_appearance_objects")
	private GiantBombObject[] firstAppearanceObjects;
	@JsonProperty("first_appearance_people")
	private GiantBombPerson[] firstAppearancePeople;
	@JsonProperty("franchises")
	private GiantBombFranchise[] franchises;
	@JsonProperty("genres")
	private GiantBombGenre[] genres;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("images")
	private GiantBombImage[] images;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("killed_characters")
	private GiantBombCharacter[] killedCharacters;
	@JsonProperty("locations")
	private GiantBombLocation[] locations;
	@JsonProperty("name")
	private String name;
	@JsonProperty("number_of_user_reviews")
	private Integer numberOfUserReviews;
	@JsonProperty("objects")
	private GiantBombObject[] objects;
	@JsonProperty("original_game_rating")
	private GiantBombGameRating[] originalGameRating;
	@JsonProperty("original_release_date")
	private Date originalReleaseDate;
	@JsonProperty("people")
	private GiantBombPerson[] people;
	@JsonProperty("platforms")
	private GiantBombPlatform[] platforms;
	@JsonProperty("publishers")
	private GiantBombCompany[] publishers;
	@JsonProperty("releases")
	private GiantBombRelease[] releases;
	@JsonProperty("dlcs")
	private GiantBombDLC[] dlcs;
	@JsonProperty("reviews")
	private GiantBombReview[] reviews;
	@JsonProperty("similar_games")
	private GiantBombGame[] similarGames;
	@JsonProperty("site_detail_url")
	private URL siteDetailUrl;
	@JsonProperty("themes")
	private GiantBombTheme[] themes;
	@JsonProperty("videos")
	private GiantBombVideo[] videos;
}
