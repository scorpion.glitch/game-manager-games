package com.scorpionglitch.gamemanager.giantbomb.objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = GiantBombGame.class, name = "game"),
    @JsonSubTypes.Type(value = GiantBombFranchise.class, name = "franchise"),
    @JsonSubTypes.Type(value = GiantBombCharacter.class, name = "character"),
    @JsonSubTypes.Type(value = GiantBombConcept.class, name = "concept"),
    @JsonSubTypes.Type(value = GiantBombObject.class, name = "object"),
    @JsonSubTypes.Type(value = GiantBombLocation.class, name = "location"),
    @JsonSubTypes.Type(value = GiantBombPerson.class, name = "person"),
    @JsonSubTypes.Type(value = GiantBombCompany.class, name = "company"),
    @JsonSubTypes.Type(value = GiantBombVideo.class, name = "video"),
})
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public abstract class GiantBombSearch {
	@JsonIgnore
	@JsonProperty("resource_type")
	private String resourceType;
}
