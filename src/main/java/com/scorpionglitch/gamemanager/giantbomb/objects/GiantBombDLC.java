package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombDLC {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("game")
	private GiantBombGame game;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("name")
	private String name;
	@JsonProperty("platform")
	private GiantBombPlatform platform;
	@JsonProperty("release_date")
	private Date releaseDate;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
}
