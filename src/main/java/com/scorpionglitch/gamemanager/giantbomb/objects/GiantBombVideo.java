package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombVideo.class)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombVideo extends GiantBombSearch {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("associations")
	private GiantBombObject[] associations;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("hd_url")
	private URL hdURL;
	@JsonProperty("high_url")
	private URL highURL;
	@JsonProperty("low_url")
	private URL lowURL;
	@JsonProperty("embed_player")
	private URL embedPlayer;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("length_seconds")
	private Integer lengthSeconds;
	@JsonProperty("name")
	private String name;
	@JsonProperty("publish_date")
	private String publishDate;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	@JsonProperty("url")
	private String url;
	@JsonProperty("user")
	private String user;
	@JsonProperty("video_categories")
	private GiantBombVideoCatagory[] videoCategories;
	@JsonProperty("video_type")
	private String videoType;
	@JsonProperty("video_show")
	private GiantBombVideoShow videoShow;
	@JsonProperty("youtube_id")
	private String youtubeID;
	@JsonProperty("saved_time")
	private Integer savedTime;
	@JsonProperty("premium")
	private Boolean premium;
	@JsonProperty("hosts")
	private String hosts;
	@JsonProperty("crew")
	private String crew;
}
