package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombReview {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("dlc")
	private GiantBombDLC dlc;
	@JsonProperty("dlc_name")
	private String dlcName;
	@JsonProperty("game")
	private GiantBombGame game;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("platforms")
	private GiantBombPlatform[] platforms;
	@JsonProperty("publish_date")
	private String publishDate;
	@JsonProperty("release")
	private GiantBombRelease release;
	@JsonProperty("reviewer")
	private String reviewer;
	@JsonProperty("score")
	private Integer score;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
}
