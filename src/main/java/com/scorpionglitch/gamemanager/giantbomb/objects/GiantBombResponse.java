package com.scorpionglitch.gamemanager.giantbomb.objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombResponse<T> {
	@JsonProperty("status_code")
	private Integer statusCode;
	@JsonProperty("error")
	private String error;
	@JsonProperty("number_of_total_results")
	private Integer numberOfTotalResults;
	@JsonProperty("number_of_page_results")
	private Integer numberOfPageResults;
	@JsonProperty("limit")
	private Integer limit;
	@JsonProperty("offset")
	private Integer offset;
	@JsonProperty("results")
	private T results;
	@JsonProperty("version")
	private String version;
	
	/*
	private Integer status_code;
	private String error;
	private Integer number_of_total_results;
	private Integer number_of_page_results;
	private Integer limit;
	private Integer offset;
	private T results;
	private String version; 
	
	public GiantBombResponse() {}

	public GiantBombResponse(Integer status_code, String error, Integer number_of_total_results, Integer number_of_page_results,
			Integer limit, Integer offset, T results, String version) {
		super();
		this.status_code = status_code;
		this.error = error;
		this.number_of_total_results = number_of_total_results;
		this.number_of_page_results = number_of_page_results;
		this.limit = limit;
		this.offset = offset;
		this.results = results;
		this.version = version;
	}

	public Integer getStatus_code() {
		return status_code;
	}

	public void setStatus_code(Integer status_code) {
		this.status_code = status_code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getNumber_of_total_results() {
		return number_of_total_results;
	}

	public void setNumber_of_total_results(Integer number_of_total_results) {
		this.number_of_total_results = number_of_total_results;
	}

	public Integer getNumber_of_page_results() {
		return number_of_page_results;
	}

	public void setNumber_of_page_results(Integer number_of_page_results) {
		this.number_of_page_results = number_of_page_results;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public T getResults() {
		return results;
	}

	public void setResults(T results) {
		this.results = results;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	*/
}
