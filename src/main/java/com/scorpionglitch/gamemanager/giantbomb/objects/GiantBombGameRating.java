package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombGameRating {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("name")
	private String name;
	@JsonProperty("rating_board")
	private GiantBombRatingBoard ratingBoard;
}
