package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class GiantBombImage {
	@JsonProperty("icon_url")
	private URL iconURL;
	@JsonProperty("medium_url")
	private URL mediumURL;
	@JsonProperty("original_url")
	private URL originalURL;
	@JsonProperty("screen_url")
	private URL screenURL;
	@JsonProperty("screen_large_url")
	private URL screenLargeURL;
	@JsonProperty("small_url")
	private URL smallURL;
	@JsonProperty("super_url")
	private URL superURL;
	@JsonProperty("thumb_url")
	private URL thumbURL;
	@JsonProperty("tiny_url")
	private URL tinyURL;
	@JsonProperty("image_tags")
	private String imageTags;
	@JsonProperty("original")
	private URL original;
	@JsonProperty("tags")
	private String tags;
}
