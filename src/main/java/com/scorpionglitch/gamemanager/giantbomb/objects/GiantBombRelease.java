package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombRelease {
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("developers")
	private GiantBombCompany[] developers;
	@JsonProperty("expected_release_day")
	private Integer expectedReleaseDay;
	@JsonProperty("expected_release_month")
	private Integer expectedReleaseMonth;
	@JsonProperty("expected_release_quarter")
	private Integer expectedReleaseQuarter;
	@JsonProperty("expected_release_year")
	private Integer expectedReleaseYear;
	@JsonProperty("game")
	private GiantBombGame game;
	@JsonProperty("game_rating")
	private GiantBombGameRating gameRating;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("images")
	private GiantBombImage[] images;
	@JsonProperty("maximum_players")
	private Integer maximumPlayers;
	@JsonProperty("minimum_players")
	private Integer minimumPlayers;
	@JsonProperty("multiPlayerFeatures")
	private GiantBombMultiplayerFeature[] multplayerFeatures;
	@JsonProperty("name")
	private String name;
	@JsonProperty("platform")
	private GiantBombPlatform platform;
	@JsonProperty("product_code_type")
	private String productCodeType;
	@JsonProperty("product_code_value")
	private String productCodeValue;
	@JsonProperty("publishers")
	private GiantBombCompany[] publishers;
	@JsonProperty("region")
	private GiantBombRegion region;
	@JsonProperty("release_date")
	private String releaseDate;
	@JsonProperty("resolutions")
	private GiantBombResolution[] resolutions;
	@JsonProperty("singlePlayerFeatures")
	private GiantBombSingleplayerFeature[] singleplayerFeatures;
	@JsonProperty("sound_systems")
	private GiantBombSoundSystem[] soundSystems;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	@JsonProperty("widescreen_support")
	private Integer widescreenSupport;
}
