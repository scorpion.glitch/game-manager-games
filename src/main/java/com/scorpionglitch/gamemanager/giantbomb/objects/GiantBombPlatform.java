package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombPlatform {
	@JsonProperty("abbreviation")
	private String abbreviation;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("company")
	private GiantBombCompany company;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("install_base")
	private String installBase;
	@JsonProperty("name")
	private String name;
	@JsonProperty("online_support")
	private Boolean onlineSupport;
	@JsonProperty("original_price")
	private String originalPrice;
	@JsonProperty("release_date")
	private String releaseDate;
	@JsonProperty("site_detail_url")
	private URL siteDetailUrl;
}
