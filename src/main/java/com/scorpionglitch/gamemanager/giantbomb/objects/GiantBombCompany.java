package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombCompany.class)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class GiantBombCompany extends GiantBombSearch {
	@JsonProperty("abbreviation")
	private String abbreviation;
	@JsonProperty("aliases")
	private String aliases;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("characters")
	private GiantBombCharacter[] characters;
	@JsonProperty("concepts")
	private GiantBombConcept[] concepts;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_founded")
	private String dateFounded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("developed_games")
	private GiantBombGame[] developedGames;
	@JsonProperty("developer_releases")
	private GiantBombRelease[] developerReleases;
	@JsonProperty("distributor_releases")
	private GiantBombRelease[] distributorReleases;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("location_address")
	private String locationAddress;
	@JsonProperty("location_city")
	private String locationCity;
	@JsonProperty("location_country")
	private String locationCountry;
	@JsonProperty("location_state")
	private String locationState;
	@JsonProperty("locations")
	private GiantBombLocation[] locations;
	@JsonProperty("name")
	private String name;
	@JsonProperty("objects")
	private GiantBombObject[] objects;
	@JsonProperty("people")
	private GiantBombPerson[] people;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("published_games")
	private GiantBombGame[] publishedGames;
	@JsonProperty("publisher_releases")
	private GiantBombRelease[] publisherReleases;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
	@JsonProperty("website")
	private URL website;
}
