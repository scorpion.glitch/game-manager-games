package com.scorpionglitch.gamemanager.giantbomb.objects;

import java.net.URL;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "resource_type", defaultImpl = GiantBombCharacter.class)
public class GiantBombCharacter extends GiantBombSearch {
	@JsonProperty("aliases")
	private String aliases;
	@JsonProperty("api_detail_url")
	private URL apiDetailURL;
	@JsonProperty("birthday")
	private String birthday;
	@JsonProperty("date_added")
	private String dateAdded;
	@JsonProperty("date_last_updated")
	private String dateLastUpdated;
	@JsonProperty("deck")
	private String deck;
	@JsonProperty("description")
	private String description;
	@JsonProperty("first_appeared_in_game")
	private GiantBombGame firstAppearedInGame;
	@JsonProperty("gender")
	private Integer gender;
	@JsonProperty("guid")
	private String guid;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("image")
	private GiantBombImage image;
	@JsonProperty("image_tags")
	private GiantBombImageTag[] imageTags;
	@JsonProperty("last_name")
	private String lastName;
	@JsonProperty("name")
	private String name;
	@JsonProperty("real_name")
	private String realName;
	@JsonProperty("site_detail_url")
	private URL siteDetailURL;
}
