package com.scorpionglitch.gamemanager.giantbomb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombCompany;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombFranchise;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGame;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGenre;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombPlatform;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombRelease;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombResponse;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class GiantBomb {
	private static final String ACCEPT = "Accept";
	private static final String USER_AGENT = "User-Agent";
	private static final String ACCEPT_VALUE = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
	private static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0";
	private static final String API_KEY = System.getenv().get("GIANTBOMB_API");
	private static final String FILE_FORMAT = "json";
	private static final String GIANTBOMB_SEARCH_GAME_URL = "https://www.giantbomb.com/api/search/?api_key=" + API_KEY + "&format=" + FILE_FORMAT + "&resources=game&query=%s";
	private static final String GIANTBOMB_RESOURCE_URL = "https://www.giantbomb.com/api/%s/%s/?api_key=" + API_KEY + "&format=" + FILE_FORMAT;
	
	public static final String COMPANY = "company";
	public static final String FRANCHISE = "franchise";
	public static final String GAME = "game";
	public static final String GENRE = "genre";
	public static final String PLATFORM = "platform";
	public static final String RELEASE = "release";
	
	private ObjectMapper objectMapper;
	
	private <T> GiantBombResponse<T> download(Class<T> cl, String urlAddress) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		URL url = new URL(urlAddress);
		
		URLConnection urlConnection = url.openConnection();
		urlConnection.setRequestProperty(ACCEPT, ACCEPT_VALUE);
		urlConnection.setRequestProperty(USER_AGENT, USER_AGENT_VALUE);
		urlConnection.connect();
		
		InputStream inputStream = urlConnection.getInputStream();
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader reader = new BufferedReader(inputStreamReader);
		
		StringBuffer buffer = new StringBuffer();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		
		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(GiantBombResponse.class, cl);
		GiantBombResponse<T> response = objectMapper.readValue(buffer.toString(),javaType);

		return response;
	}
	
	public GiantBombGame[] searchGames(String query) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		return download(GiantBombGame[].class, String.format(GIANTBOMB_SEARCH_GAME_URL, query)).getResults();
	}
	
	public GiantBombCompany getCompany(String companyID)
		throws JsonParseException, JsonMappingException, IOException 
	{
		return download(GiantBombCompany.class, String.format(GIANTBOMB_RESOURCE_URL, COMPANY, companyID)).getResults();
	}
	
	public GiantBombFranchise getFranchise(String franchiseID)
		throws JsonParseException, JsonMappingException, IOException
	{
		return download(GiantBombFranchise.class, String.format(GIANTBOMB_RESOURCE_URL, FRANCHISE, franchiseID)).getResults();
	}
	
	public GiantBombGame getGame(String gameID) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		return download(GiantBombGame.class, String.format(GIANTBOMB_RESOURCE_URL, GAME, gameID)).getResults();
	}
	
	public GiantBombGenre getGenre(String genreID) 
		throws JsonParseException, JsonMappingException, IOException
	{
		return download(GiantBombGenre.class, String.format(GIANTBOMB_RESOURCE_URL, GENRE, genreID)).getResults();
	}
	
	public GiantBombPlatform getPlatform(String platformID) 
		throws JsonParseException, JsonMappingException, IOException
	{
		return download(GiantBombPlatform.class, String.format(GIANTBOMB_RESOURCE_URL, PLATFORM, platformID)).getResults();
	}
	
	public GiantBombRelease getRelease(String releaseID)
		throws JsonParseException, JsonMappingException, IOException
	{
		return download(GiantBombRelease.class, String.format(GIANTBOMB_RESOURCE_URL, RELEASE, releaseID)).getResults();
	}
	
	
}
