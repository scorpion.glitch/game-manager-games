package com.scorpionglitch.gamemanager.tools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.scorpionglitch.gamemanager.giantbomb.GiantBomb;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGame;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombPlatform;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombRelease;
import com.scorpionglitch.gamemanager.objects.Company;
import com.scorpionglitch.gamemanager.objects.Franchise;
import com.scorpionglitch.gamemanager.objects.Game;
import com.scorpionglitch.gamemanager.objects.Genre;
import com.scorpionglitch.gamemanager.objects.Platform;
import com.scorpionglitch.gamemanager.objects.GMRelease;
import com.scorpionglitch.gamemanager.objects.GMUser;
import com.scorpionglitch.gamemanager.repositories.CompanyRepository;
import com.scorpionglitch.gamemanager.repositories.FranchiseRepository;
import com.scorpionglitch.gamemanager.repositories.GMUserRepository;
import com.scorpionglitch.gamemanager.repositories.GameRepository;
import com.scorpionglitch.gamemanager.repositories.GenreRepository;
import com.scorpionglitch.gamemanager.repositories.PlatformRepository;
import com.scorpionglitch.gamemanager.repositories.ReleaseRepository;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class GameManager {
	private CompanyRepository companies;
	private FranchiseRepository franchises;
	private GameRepository games;
	private GenreRepository genres;
	private PlatformRepository platforms;
	private ReleaseRepository releases;
	private GMUserRepository users;
	
	private GiantBomb giantBomb;
	
	public GiantBombGame[] search(String query) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		return giantBomb.searchGames(query);
	}
	
	public Optional<Company> getCompany(String companyGUID) {
		return companies.findById(companyGUID);
	}
	public Optional<Franchise> getFranchise(String franchiseGUID) {
		return franchises.findById(franchiseGUID);
	}
	public Optional<Game> getGame(String gameGUID) {
		return games.findById(gameGUID);
	}
	public Optional<Genre> getGenre(String genreGUID) {
		return genres.findById(genreGUID);
	}
	public Optional<Platform> getPlatform(String platformGUID) {
		return platforms.findById(platformGUID);
	}
	
	public Page<Game> getGames(int page, int size) {
		return games.findAll(PageRequest.of(page, size, Sort.by("name")));
	}
	public Page<Platform> getPlatform(int page, int size) {
		return platforms.findAll(PageRequest.of(page, size, Sort.by("name")));
	}
		
	public GMRelease importRelease(String releaseGUID) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		Optional<GMRelease> storedRelease = releases.findById(releaseGUID);
		GMRelease release = storedRelease.orElse(null);
		if (release == null) {
			GiantBombRelease gbRelease = giantBomb.getRelease(releaseGUID);
			GiantBombGame gbGame = null;
			try {
				String regex = "https:\\/\\/www\\.giantbomb\\.com\\/api\\/game\\/(?<gameGUID>\\d+-\\d+)\\/";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(gbRelease.getGame().getApiDetailURL().toString());
				matcher.matches();
				String gameGUID = matcher.group("gameGUID");
				gbGame = giantBomb.getGame(gameGUID);
			} catch (FileNotFoundException fnfe) {
				System.out.println(gbRelease.getGame());
				return null;
			}
			gbRelease.setGame(gbGame);
			if (gbRelease.getDevelopers() == null) {
				gbRelease.setDevelopers(gbGame.getDevelopers());
			}
			if (gbRelease.getPublishers() == null) {
				gbRelease.setPublishers(gbGame.getPublishers());
			}
			if (gbRelease.getDevelopers() != null) {
				for (int i = 0; i < gbRelease.getDevelopers().length; i++) {
					String developerGUID = "3010-" + gbRelease.getDevelopers()[i].getId();
					gbRelease.getDevelopers()[i] = giantBomb.getCompany(developerGUID);
				}
			}
			if (gbGame.getFranchises() != null) {
				for (int i = 0; i < gbGame.getFranchises().length; i++) {
					String franchiseGUID = "3025-" + gbGame.getFranchises()[i].getId();
					gbGame.getFranchises()[i] = giantBomb.getFranchise(franchiseGUID);
				}
			}
			if (gbGame.getGenres() != null) {
				for (int i = 0; i < gbGame.getGenres().length; i++) {
					String genreGUID = "3060-" + gbGame.getGenres()[i].getId();
					gbGame.getGenres()[i] = giantBomb.getGenre(genreGUID);
				}
			}
			if (gbRelease.getPublishers() != null) {
				for (int i = 0; i < gbRelease.getPublishers().length; i++) {
					String publisherGUID = "3010-" + gbRelease.getPublishers()[i].getId();
					gbRelease.getPublishers()[i] = giantBomb.getCompany(publisherGUID);
				}
			}
			String platformGUID = "3045-" + gbRelease.getPlatform().getId();
			gbRelease.setPlatform(giantBomb.getPlatform(platformGUID));
			gbRelease.getPlatform().setCompany(giantBomb.getCompany("3010-" + gbRelease.getPlatform().getCompany().getId()));
			release = Converter.convert(gbRelease);
			
			for (Company developer : release.getDevelopers()) {
				companies.save(developer);
			}
			for (Company publisher : release.getPublishers()) {
				companies.save(publisher);
			}
			for (Franchise franchise : release.getFranchises()) {
				franchises.save(franchise);
			}
			for (Genre genre : release.getGenres()) {
				genres.save(genre);
			}
			companies.save(release.getPlatform().getCompany());
			platforms.save(release.getPlatform());
			
			release = releases.save(release); 
		}
		return release;
	}

	public Platform importPlatform(String platformID) 
		throws JsonParseException, JsonMappingException, IOException 
	{
		Optional<Platform> storedPlatform = platforms.findById(platformID);
		Platform platform = storedPlatform.orElse(null);
		if (platform == null) {
			GiantBombPlatform giantBombPlatform = giantBomb.getPlatform(platformID);
			giantBombPlatform.setCompany(giantBomb.getCompany("3010-" + giantBombPlatform.getCompany().getId()));
			
			platform = Converter.convert(giantBombPlatform);
			platform = platforms.save(platform);
		}
		return platform;
	}

	public Page<GMUser> getUsers() {
		return users.findAll(PageRequest.of(0, 10, Sort.by("username")));
	}
	
	public GMUser addTestUser() {
		GMUser user = new GMUser();
		user.setUsername("scorpion_glitch");
		user.setPasswordHash("passwordhash");
		return users.save(user);
	}
}
