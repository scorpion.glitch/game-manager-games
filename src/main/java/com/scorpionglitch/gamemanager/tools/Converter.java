package com.scorpionglitch.gamemanager.tools;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombCompany;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombFranchise;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGame;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGenre;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombPlatform;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombRelease;
import com.scorpionglitch.gamemanager.objects.Company;
import com.scorpionglitch.gamemanager.objects.Franchise;
import com.scorpionglitch.gamemanager.objects.Game;
import com.scorpionglitch.gamemanager.objects.Genre;
import com.scorpionglitch.gamemanager.objects.Platform;
import com.scorpionglitch.gamemanager.objects.GMRelease;

public class Converter {
	public static Game convert(GiantBombGame giantBombRelease) {
		Game gameManagerGame = new Game();
		
		gameManagerGame.setGiantbombID(giantBombRelease.getGuid());
		gameManagerGame.setAliases(giantBombRelease.getAliases());
		gameManagerGame.setDeck(giantBombRelease.getDeck());
		gameManagerGame.setDescription(giantBombRelease.getDescription());
		gameManagerGame.setName(giantBombRelease.getName());
		
		/*
		gameManagerGame.setCoverImage(giantBombRelease.getImage().getOriginal());
		Set<Company> developers = new HashSet<>();
		Arrays.asList(giantBombRelease.getPublishers()).stream().map(Converter::convert).forEach(developers::add);
		gameManagerGame.setDevelopers(developers);
		Set<Company> publishers = new HashSet<>();
		Arrays.asList(giantBombRelease.getPublishers()).stream().map(Converter::convert).forEach(publishers::add);
		gameManagerGame.setPublishers(publishers);
		
		Set<GameManagerFranchise> franchises = new HashSet<>();
		Arrays.asList(giantBombRelease.getGame().getFranchises()).stream().map(Converter::convert).forEach(franchises::add);
		gameManagerGame.setFranchises(franchises);
		gameManagerGame.setGameRating(giantBombRelease.getGameRating().getName());
		*/
		
		HashSet<Genre> genres = new HashSet<>();
		Arrays.asList(giantBombRelease.getGenres()).stream().map(Converter::convert).forEach(genres::add);
		gameManagerGame.setGenres(genres);
		
		gameManagerGame.setGiantbombDetailURL(giantBombRelease.getSiteDetailUrl());
		
		return gameManagerGame;
	}
	
	public static GMRelease convert(GiantBombRelease giantBombRelease) {
		GMRelease release = new GMRelease();
		
		release.setGiantbombID(giantBombRelease.getGame().getGuid());
		release.setPlatform(convert(giantBombRelease.getPlatform()));
		
		release.setCoverImage(giantBombRelease.getImage().getOriginal());
		HashSet<Company> developers = new HashSet<>();
		Arrays.asList(giantBombRelease.getPublishers()).stream().map(Converter::convert).forEach(developers::add);
		release.setDevelopers(developers);
		HashSet<Company> publishers = new HashSet<>();
		Arrays.asList(giantBombRelease.getPublishers()).stream().map(Converter::convert).forEach(publishers::add);
		release.setPublishers(publishers);
		
		HashSet<Franchise> franchises = new HashSet<>();
		Arrays.asList(giantBombRelease.getGame().getFranchises()).stream().map(Converter::convert).forEach(franchises::add);
		release.setFranchises(franchises);
		release.setGameRating(giantBombRelease.getGameRating().getName());
		
		Set<Genre> genres = new HashSet<>();
		Arrays.asList(giantBombRelease.getGame().getGenres()).stream().map(Converter::convert).forEach(genres::add);
		release.setGenres(genres);
		
		
		release.setPlatform(convert(giantBombRelease.getPlatform()));
		
		
		
		release.setRegion(giantBombRelease.getRegion().getName());
		release.setReleaseDate(giantBombRelease.getReleaseDate());
		release.setGiantbombDetailURL(giantBombRelease.getSiteDetailURL());
		
		return release;
	}
	
	public static Platform convert(GiantBombPlatform giantBombPlatform) {
		Platform gameManagerPlatform = new Platform();
		
		gameManagerPlatform.setGiantbombID(giantBombPlatform.getGuid());
		gameManagerPlatform.setAbbreviation(giantBombPlatform.getAbbreviation());
		gameManagerPlatform.setCompany(convert(giantBombPlatform.getCompany()));
		gameManagerPlatform.setDeck(giantBombPlatform.getDeck());
		gameManagerPlatform.setDescription(giantBombPlatform.getDescription());
		gameManagerPlatform.setImage(giantBombPlatform.getImage().getOriginal());
		gameManagerPlatform.setName(giantBombPlatform.getName());
		gameManagerPlatform.setOriginalPrice(giantBombPlatform.getOriginalPrice());
		gameManagerPlatform.setReleaseDate(giantBombPlatform.getReleaseDate());
		gameManagerPlatform.setSiteDetailUrl(giantBombPlatform.getSiteDetailUrl());
		
		return gameManagerPlatform;
	}
	
	public static Genre convert(GiantBombGenre giantBombGenre) {
		Genre genre = new Genre();
		
		genre.setGiantbombID(giantBombGenre.getGuid());
		genre.setDeck(giantBombGenre.getDeck());
		genre.setDescription(giantBombGenre.getDescription());
		genre.setImage(giantBombGenre.getImage().getOriginal());
		genre.setName(giantBombGenre.getName());
		
		return genre;
	}
	
	public static Franchise convert(GiantBombFranchise giantBombFranchise) {
		Franchise franchise = new Franchise();
		
		franchise.setGiantbombID(giantBombFranchise.getGuid());
		franchise.setAliases(giantBombFranchise.getAliases());
		franchise.setDeck(giantBombFranchise.getDeck());
		franchise.setDescription(giantBombFranchise.getDescription());
		franchise.setImage(giantBombFranchise.getImage().getOriginal());
		franchise.setName(giantBombFranchise.getName());
		franchise.setSiteDetailURL(giantBombFranchise.getSiteDetailURL());
		
		return franchise;
	}
	
	public static Company convert(GiantBombCompany giantBombCompany) {
		Company company = new Company();
		
		company.setGiantbombID(giantBombCompany.getGuid());
		company.setAbbreviation(giantBombCompany.getAbbreviation());
		company.setAliases(giantBombCompany.getAliases());
		company.setDateFounded(giantBombCompany.getDateFounded());
		company.setDeck(giantBombCompany.getDeck());
		company.setDescription(giantBombCompany.getDescription());
		company.setImage(giantBombCompany.getImage().getOriginalURL());
		company.setLocationAddress(giantBombCompany.getLocationAddress());
		company.setLocationCity(giantBombCompany.getLocationCity());
		company.setLocationCountry(giantBombCompany.getLocationCountry());
		company.setLocationState(giantBombCompany.getLocationState());
		company.setName(giantBombCompany.getName());
		company.setPhone(giantBombCompany.getPhone());
		company.setSiteDetailURL(giantBombCompany.getSiteDetailURL());
		company.setWebsite(giantBombCompany.getWebsite());
		
		return company;
	}
}
