package com.scorpionglitch.gamemanager.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.scorpionglitch.gamemanager.objects.Platform;

public interface PlatformRepository extends PagingAndSortingRepository<Platform, String> {
	Optional<Platform> findByName(String name);
}
