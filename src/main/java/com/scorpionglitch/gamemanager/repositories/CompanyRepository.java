package com.scorpionglitch.gamemanager.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.scorpionglitch.gamemanager.objects.Company;

@RepositoryRestResource(collectionResourceRel = "companies", path = "companies")
public interface CompanyRepository extends PagingAndSortingRepository<Company, String> {
	Optional<Company> findByName(String name);
}
