package com.scorpionglitch.gamemanager.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.scorpionglitch.gamemanager.objects.Franchise;

public interface FranchiseRepository extends PagingAndSortingRepository<Franchise, String> {
	Optional<Franchise> findByName(String name);
}
