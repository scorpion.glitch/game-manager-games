package com.scorpionglitch.gamemanager.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.scorpionglitch.gamemanager.objects.GMUser;

public interface GMUserRepository extends PagingAndSortingRepository<GMUser, String> {

}
