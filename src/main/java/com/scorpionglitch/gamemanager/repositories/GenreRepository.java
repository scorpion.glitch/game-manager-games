package com.scorpionglitch.gamemanager.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.scorpionglitch.gamemanager.objects.Genre;

public interface GenreRepository extends PagingAndSortingRepository<Genre, String> {
	Optional<Genre> findByName(String name);
}
