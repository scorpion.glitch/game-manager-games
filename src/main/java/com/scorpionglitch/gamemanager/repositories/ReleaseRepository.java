package com.scorpionglitch.gamemanager.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.scorpionglitch.gamemanager.objects.GMRelease;

@RepositoryRestResource(collectionResourceRel = "releases", path = "releases")
public interface ReleaseRepository  extends PagingAndSortingRepository<GMRelease, String>  {

}
