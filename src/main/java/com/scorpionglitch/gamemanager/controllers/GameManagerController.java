package com.scorpionglitch.gamemanager.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scorpionglitch.gamemanager.giantbomb.objects.GiantBombGame;
import com.scorpionglitch.gamemanager.objects.Company;
import com.scorpionglitch.gamemanager.objects.Franchise;
import com.scorpionglitch.gamemanager.objects.Game;
import com.scorpionglitch.gamemanager.objects.Genre;
import com.scorpionglitch.gamemanager.objects.Platform;
import com.scorpionglitch.gamemanager.objects.GMRelease;
import com.scorpionglitch.gamemanager.objects.GMUser;
import com.scorpionglitch.gamemanager.tools.GameManager;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api/gamemanager")
public class GameManagerController {
	
	private ObjectMapper objectMapper;
	private GameManager gameManager;
	
	{
		objectMapper = new ObjectMapper();
		objectMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
	}
	
	@GetMapping("/addtestuser")
	public ResponseEntity<GMUser> addTestUser() {
		return ResponseEntity.ok(gameManager.addTestUser());
	}
	
	@GetMapping("/users")
	public ResponseEntity<Page<GMUser>> getUsers() {
		return ResponseEntity.ok(gameManager.getUsers());
	}
	
	@PostMapping("/import/platform/{platformID}")
	public ResponseEntity<Platform> importPlatform(@PathVariable String platformID)
		throws JsonParseException, JsonMappingException, MalformedURLException, IOException
	{
		return ResponseEntity.ok(gameManager.importPlatform(platformID));
	}
		
	@PostMapping("/import/release/{releaseID}")
	public ResponseEntity<GMRelease> importRelease(@PathVariable String releaseID) 
		throws JsonParseException, JsonMappingException, MalformedURLException, IOException 
	{
		return ResponseEntity.ok(gameManager.importRelease(releaseID));
	}
	
	@GetMapping("/searchgame/{query}")
	public ResponseEntity<GiantBombGame[]> searchForGame(@PathVariable String query) 
		throws JsonParseException, JsonMappingException, IOException
	{
		return ResponseEntity.ok(gameManager.search(query));
	}
	
	@GetMapping("/games")
	public ResponseEntity<Page<Game>> getGames(
		@RequestParam(required = true, defaultValue = "0") int page, 
		@RequestParam(required = true, defaultValue = "10") int size) 
	{
		return ResponseEntity.ok(gameManager.getGames(page, size));
	}
	
	@GetMapping("/company/{companyID}")
	public ResponseEntity<Company> getCompany(@PathVariable String companyID, HttpServletResponse response) {
		Optional<Company> company = gameManager.getCompany(companyID);
		if (!company.isPresent()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return ResponseEntity.ok(company.orElse(new Company()));
	}
	
	@GetMapping("/franchise/{franchiseID}")
	public ResponseEntity<Franchise> getFranchise(@PathVariable String franchiseID, HttpServletResponse response) {
		Optional<Franchise> franchise = gameManager.getFranchise(franchiseID);
		if (!franchise.isPresent()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return ResponseEntity.ok(franchise.orElse(new Franchise()));
	}
	
	@GetMapping("/game/{gameID}")
	public ResponseEntity<Game> getGame(@PathVariable String gameID, HttpServletResponse response) {
		Optional<Game> game = gameManager.getGame(gameID);
		if (!game.isPresent()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return ResponseEntity.ok(game.orElse(new Game()));
	}
	
	@GetMapping("/genre/{genreID}")
	public ResponseEntity<Genre> getGenre(@PathVariable String genreID, HttpServletResponse response) {
		Optional<Genre> genre = gameManager.getGenre(genreID);
		if (!genre.isPresent()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return ResponseEntity.ok(genre.orElse(new Genre()));
	}
	
	@GetMapping("/platform/{platformID}")
	public ResponseEntity<Platform> getPlatform(@PathVariable String platformID, HttpServletResponse response) {
		Optional<Platform> platform = gameManager.getPlatform(platformID);
		if (!platform.isPresent()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return ResponseEntity.ok(platform.orElse(new Platform()));
	}
	
}
